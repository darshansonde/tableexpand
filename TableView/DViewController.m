//
//  DViewController.m
//  TableView
//
//  Created by Darshan Sonde on 06/05/13.
//  Copyright (c) 2013 Darshan Sonde. All rights reserved.
//

#import "DViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface DViewController ()<UITableViewDataSource,UITableViewDelegate> {
    UIImageView *_topImageView;
    UIImageView *_bottomImageView;
    CGPoint     _origPos;
    CGRect      _topStartRect;
    CGRect      _bottomStartRect;
}

@end

@implementation DViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.stackTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - table view methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.tableView)
        return 10;
    else {
        return 5;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView) {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.textLabel.text = [NSString stringWithFormat:@"Expand Cell %d",indexPath.row];
        return cell;
    }else {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.textLabel.text = [NSString stringWithFormat:@"Embedded Table View %d",indexPath.row];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableView) {
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        
        //take top cells screenshot
        CGRect topFrame = CGRectMake(0,tableView.contentOffset.y,
                                     tableView.frame.size.width,selectedCell.frame.origin.y-tableView.contentOffset.y);
        
        CGSize imageSize = topFrame.size;
        
        UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0, -tableView.contentOffset.y);
        
        [[tableView layer] renderInContext:context];
        
        CGContextRestoreGState(context);
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        topFrame = [self.view convertRect:topFrame fromView:tableView];
        _topStartRect = topFrame;
        _topImageView = [[UIImageView alloc] initWithFrame:topFrame];
        _topImageView.image = image;
        [self.view addSubview:_topImageView];
        
        UIGraphicsEndImageContext();
        
        
        
        
        //take bottom cells screenshot
        CGRect bottomFrame = CGRectMake(tableView.frame.origin.x,selectedCell.frame.origin.y+selectedCell.frame.size.height,
                                        tableView.frame.size.width,CGRectGetHeight(tableView.frame)-(selectedCell.frame.origin.y-tableView.contentOffset.y+selectedCell.frame.size.height));
        imageSize = bottomFrame.size;
        
        UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
        
        context = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(context);
        CGContextTranslateCTM(context, 0, -selectedCell.frame.origin.y-selectedCell.frame.size.height);
        [[tableView layer] renderInContext:context];
        
        CGContextRestoreGState(context);
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        bottomFrame = [self.view convertRect:bottomFrame fromView:tableView];
        _bottomStartRect = bottomFrame;
        _bottomImageView = [[UIImageView alloc] initWithFrame:bottomFrame];
        _bottomImageView.image = image;
        [self.view addSubview:_bottomImageView];
        
        UIGraphicsEndImageContext();
        
        //animate the cells and add new tableview
        [self.view insertSubview:self.stackTable belowSubview:_topImageView];
        _origPos = [self.view convertPoint:selectedCell.frame.origin fromView:tableView];
        self.stackTable.frame = CGRectOffset(self.view.bounds,_origPos.x,_origPos.y);
        self.tableView.hidden = YES;
        [UIView animateWithDuration:1.0f animations:^{
            _topImageView.frame = CGRectOffset(topFrame, 0, -topFrame.origin.y-topFrame.size.height);
            _bottomImageView.frame = CGRectOffset(bottomFrame, 0, self.view.bounds.size.height-bottomFrame.origin.y);
            self.stackTable.frame = self.view.bounds;
        }completion:^(BOOL finished) {
            
        }];
    }
    else if(self.stackTable.superview != nil) {
        //restore previous state
        [UIView animateWithDuration:1.0f animations:^{
            self.stackTable.frame = CGRectOffset(self.view.bounds,_origPos.x,_origPos.y);
            _topImageView.frame = _topStartRect;
            _bottomImageView.frame = _bottomStartRect;
        } completion:^(BOOL finished) {
            [self.stackTable removeFromSuperview];
            self.tableView.hidden = NO;
            [_topImageView removeFromSuperview];
            [_bottomImageView removeFromSuperview];
            _topImageView =nil;
            _bottomImageView = nil;
        }];
    }
}
@end
