//
//  DAppDelegate.h
//  TableView
//
//  Created by Darshan Sonde on 06/05/13.
//  Copyright (c) 2013 Darshan Sonde. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DViewController;

@interface DAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DViewController *viewController;

@end
