//
//  DViewController.h
//  TableView
//
//  Created by Darshan Sonde on 06/05/13.
//  Copyright (c) 2013 Darshan Sonde. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITableView *stackTable;

@end
