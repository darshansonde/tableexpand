//
//  main.m
//  TableView
//
//  Created by Darshan Sonde on 06/05/13.
//  Copyright (c) 2013 Darshan Sonde. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DAppDelegate class]));
    }
}
